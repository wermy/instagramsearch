//
//  SearchResultsViewController.m
//  InstagramSearch
//
//  Created by Warner Skoch on 7/16/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import "SearchViewController.h"
#import "InstagramPhotoCell.h"
#import "AFNetworking/AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "InstagramPhoto.h"
#import "FXBlurView/FXBlurView.h"
#import "YIInnerShadowView/YIInnerShadowView.h"

@interface SearchViewController ()<UIAlertViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet YIInnerShadowView *shadowView;
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;

@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) NSString *nextSearchURL;
@property (nonatomic, strong) FXBlurView *blurView;
@property (nonatomic, strong) UIImageView *largeImage;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@property (nonatomic, strong) UIBarButtonItem *searchButton;
@property (nonatomic, strong) UIAlertView *alert;
@end

@implementation SearchViewController

#pragma mark - Misc methods
- (void)loadResultsFromURL:(NSString *)url{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Results: %@", responseObject);
        self.nextSearchURL = responseObject[@"pagination"][@"next_url"];
        for (NSDictionary *result in responseObject[@"data"]) {
            [self.searchResults addObject:[[InstagramPhoto alloc] initWithURL:result[@"images"][@"standard_resolution"][@"url"]]];
        }
        [self refreshResults];
        if (self.searchResults.count == 0) {
            [self.statusLabel setHidden:NO];
            [self.statusLabel setText:@"No results found.\nTry searching for another tag."];
        }else{
            [self.statusLabel setHidden:YES];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.statusLabel setText:@""];
        [[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil] show];
    }];
}

- (void)dismissLargeImage{
    InstagramPhotoCell *cell = (InstagramPhotoCell *)[self collectionView:self.collectionView cellForItemAtIndexPath:self.selectedIndexPath];
    CGRect startFrame = [self.view convertRect:cell.frame fromView:self.collectionView];
    [UIView animateWithDuration:0.2f animations:^{
        [self.largeImage setFrame:startFrame];
        [self.blurView setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [cell.photoView setHidden:NO];
        [self.blurView removeFromSuperview];
        [self.largeImage removeFromSuperview];
        self.blurView = nil;
        self.largeImage = nil;
        [self.searchButton setEnabled:YES];
    }];
}

- (void)searchForTag:(NSString *)tag{
    if (tag.length > 0) {
        [self.statusLabel setText:@"Searching..."];
        self.searchResults = [NSMutableArray new];
        self.nextSearchURL = nil;
        NSString *searchTerm = [tag stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *searchURL = [NSString stringWithFormat:@"https://api.instagram.com/v1/tags/%@/media/recent?client_id=71ef0945b9234b5c82b7748a5931cb0c", searchTerm];
        [self loadResultsFromURL:searchURL];
    }
}

- (void)refreshResults{
    [self.collectionView reloadData];
    
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self searchForTag:textField.text];
    if (self.alert) {
        [self.alert dismissWithClickedButtonIndex:0 animated:YES];
        self.alert = nil;
    }
    return NO;
}
#pragma mark - IBActions
- (IBAction)tappedSearch:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Enter a tag to search for:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Search", nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alert show];
    [[alert textFieldAtIndex:0] setDelegate:self];
    self.alert = alert;
}

#pragma - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex != alertView.cancelButtonIndex) {
        NSString *tag = [alertView textFieldAtIndex:0].text;
        [self searchForTag:tag];
    }
    self.alert = nil;
}

#pragma mark - UICollectionView methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.searchResults.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    if (!self.blurView) {
        
        [self.searchButton setEnabled:NO];
        
        self.selectedIndexPath = indexPath;
        InstagramPhoto *photo = self.searchResults[self.selectedIndexPath.item];
        InstagramPhotoCell *cell = (InstagramPhotoCell *)[self collectionView:collectionView cellForItemAtIndexPath:self.selectedIndexPath];
        [cell.photoView setHidden:YES];
        
        self.blurView = [[FXBlurView alloc] initWithFrame:self.view.bounds];
        [self.blurView setAlpha:0.0f];
        [self.blurView setDynamic:YES];
        [self.blurView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissLargeImage)]];
        [self.blurView setTintColor:[UIColor colorWithRed:10.0f/255.0f green:10.0f/255.0f blue:10.0f/255.0f alpha:1.0f]];
        [self.view addSubview:self.blurView];
        

        
        CGRect startFrame = [self.view convertRect:cell.frame fromView:self.collectionView];
        self.largeImage = [[UIImageView alloc] initWithFrame:startFrame];
        [self.largeImage setImageWithURL:[NSURL URLWithString:photo.url]];
        [self.largeImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissLargeImage)]];
        [self.largeImage setContentMode:UIViewContentModeScaleAspectFit];
        [self.view addSubview:self.largeImage];
        
        
        [UIView animateWithDuration:0.3f animations:^{
            [self.blurView setAlpha:1.0f];
            [self.largeImage setFrame:CGRectMake(0, (self.view.frame.size.height - self.view.frame.size.width)/2.0f, self.view.frame.size.width, self.view.frame.size.width)];
        }];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    InstagramPhotoCell *cell = (InstagramPhotoCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:[InstagramPhotoCell reuseIdentifier] forIndexPath:indexPath];
    [cell setupWithInstagramPhoto:self.searchResults[indexPath.item]];
    if (indexPath.item >= (self.searchResults.count-1) && self.nextSearchURL.length > 0) {
        [self loadResultsFromURL:self.nextSearchURL];
    }
    return cell;
}

#pragma mark - Object/View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.collectionView registerNib:[UINib nibWithNibName:[InstagramPhotoCell reuseIdentifier] bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:[InstagramPhotoCell reuseIdentifier]];
    
    self.searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(tappedSearch:)];
    [self.navigationItem setRightBarButtonItem:self.searchButton];
    
    [self.shadowView setShadowRadius:30.0f];
    [self.shadowView setShadowOpacity:0.5f];
    
    [self.statusLabel setText:@"Press the search button to get started!"];
    
    [self setTitle:@"Instagram Search"];
}

- (BOOL)prefersStatusBarHidden{
    return YES;
}

@end
