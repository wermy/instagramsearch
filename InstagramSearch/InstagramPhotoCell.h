//
//  InstagramPhotoCell.h
//  InstagramSearch
//
//  Created by Warner Skoch on 7/16/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import <UIKit/UIKit.h>

@class InstagramPhoto;

@interface InstagramPhotoCell : UICollectionViewCell

+ (NSString *)reuseIdentifier;
- (UIImageView *)photoView;
- (void)setupWithInstagramPhoto:(InstagramPhoto *)photo;
@end
