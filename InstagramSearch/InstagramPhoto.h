//
//  InstagramPhoto.h
//  InstagramSearch
//
//  Created by Warner Skoch on 7/16/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramPhoto : NSObject
@property (nonatomic, strong, readonly) NSString *url;
- (id)initWithURL:(NSString *)url;
@end
