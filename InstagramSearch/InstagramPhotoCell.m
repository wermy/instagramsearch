//
//  InstagramPhotoCell.m
//  InstagramSearch
//
//  Created by Warner Skoch on 7/16/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import "InstagramPhotoCell.h"
#import "InstagramPhoto.h"
#import "UIImageView+AFNetworking.h"

@interface InstagramPhotoCell()
@property (nonatomic, strong) IBOutlet UIImageView *image;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation InstagramPhotoCell
+ (NSString *)reuseIdentifier{
    return NSStringFromClass(self);
}

- (UIImageView *)photoView{
    return self.image;
}

- (void)setupWithInstagramPhoto:(InstagramPhoto *)photo{
    [self.image setImage:nil];
    [self.activityIndicator setHidden:NO];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:photo.url]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    __block InstagramPhotoCell *blockSelf = self;
    [self.image setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [blockSelf.image setImage:image];
        [blockSelf.activityIndicator setHidden:YES];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        [blockSelf.activityIndicator setHidden:YES];
    }];
}
@end
