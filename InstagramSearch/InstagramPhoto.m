//
//  InstagramPhoto.m
//  InstagramSearch
//
//  Created by Warner Skoch on 7/16/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import "InstagramPhoto.h"
@implementation InstagramPhoto
- (id)initWithURL:(NSString *)url{
    self = [super init];
    if (self) {
        _url = url;
    }
    return self;
}

- (NSString *)description{
    return self.url;
}
@end
